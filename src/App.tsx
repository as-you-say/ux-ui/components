import React from 'react';
import KanbanBoard from './KanbanBoard';

function App() {
  return (
    <>
      <header></header>
      <aside></aside>
      <section>
        <KanbanBoard />
      </section>
    </>
  );
}

export default App;
