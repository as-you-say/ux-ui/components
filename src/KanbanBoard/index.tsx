import React from 'react';
import { useEffect } from "react";
import "./style.css";

export default function KanbanBoard() {
  return (
    <div className="row">
      <Board/>
      <Board/>
      <Board/>
      <Board/>
    </div>
  )
}

function Board() {
  return (
    <div className="col-xs-12 col-md-3">
      <div className="board">
        <h2 className="title">Board</h2>
        <ul className="list u-fancy-scrollbar">
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
        </ul>
      </div>
    </div>
  )
}

function Card () {
  return (
    <li className="card">
      <h3 className="title">Card</h3>
    </li>
  )
}